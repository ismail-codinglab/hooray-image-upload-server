const base64Img = require('base64-img');
const Hashids = require('hashids');
const imageHashids = new Hashids('ImageUploadProject',5,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [
        
    ],
    create: [
    ],
    update: [],
    patch: [],
    remove: []
},

after: {
    all: [
        (hook) => {
            return new Promise((res,rej) =>{
                // console.log("ALLHOOK",hook);
                fn = (element)=>{
                    return new Promise((resolve,reject) =>{
                        let hash = imageHashids.encode(element.id);
                        let imageName = element.filename;
                        base64Img.base64(`./uploads/${hash}/${imageName}`, function(err, data) {
                            if (err) {
                                console.log("error loading file",err);
                                return;
                            }
                            element.file = data;
                            resolve(element);
        
                        })
                    });
                };
                if(!hook.result.data || !hook.result.data.length){
                    res(hook);
                }
                Promise.all(hook.result.data.map(fn)).then((all)=>{
                    hook.result.data = all;
                    res(hook);
                });

            })
        }
    ],
    find: [],
    get: [
        (hook) => {
            console.log("HOOK",hook);
            return new Promise((resolve,reject) =>{
                let hash = imageHashids.encode(hook.result.id);
                let imageName = hook.result.filename;
                base64Img.base64(`./uploads/${hash}/${imageName}`, function(err, data) {
                    if (err) {
                        console.log("error loading file",err);
                        return;
                    }
                    hook.result.file = data;
                    resolve();

                })
            })
        }
    ],
    create: [
        
        (hook,next) => {
            let imageId = hook.result.id;
            let imageName = hook.result.filename;
            let base64ImageString = hook.data.file;
            let hash = imageHashids.encode(imageId);
            base64Img.img(
                base64ImageString, //base64string
                `./uploads/${hash}/`,//savepath
                imageName.replace(/\.[^/.]+$/, ""), //filename
                function (err, filepath) {//callback
                    if (err) {
                        console.log("error uploading file",err);
                        return;
                    }
                });
            next();
        }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
